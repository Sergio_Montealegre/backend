from rest_framework import generics
from rest_framework.response import Response

from hotelApp.models.reserva import Reserva 
from hotelApp.serializers.reservaSerializer import HabReservadaSerializer

class HabReservadaView(generics.RetrieveAPIView):
    def get(self, request, checkin, checkout):
        queryset = Reserva.objects.values('habitacion_id', 'checkIn', 'checkOut').filter(checkOut__gte = checkin, checkIn__lte = checkout)
        serializer = HabReservadaSerializer(queryset, many=True)
        return Response(serializer.data)