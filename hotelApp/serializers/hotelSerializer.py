from hotelApp.models.hotel import Hotel
from rest_framework import serializers

class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ['id', 'nombreHotel', 'ciudad', 'direccion', 
                'correo', 'telefono', 'descripcion']