from rest_framework import status, views
from rest_framework.response import Response

from hotelApp.models.reserva import Reserva
from hotelApp.serializers.reservaSerializer import ReservaSerializer 

class CancelarReservaView(views.APIView):
    def delete(self, request, idreserva):
        try:
            reserva = Reserva.objects.get(id=idreserva)
        except Reserva.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        data = ReservaSerializer(reserva, many=False).data
        reserva.delete()
        return Response(data=data, status=status.HTTP_200_OK)