from rest_framework import generics
from rest_framework.response import Response

from hotelApp.models.reserva import Reserva
from hotelApp.serializers.reservaSerializer import ReservaSerializer 
from hotelApp.serializers.reservaSerializer import UltReservaSerializer
from hotelApp.serializers.reservaSerializer import ListMaestReservasSerializer

class VerReservasView(generics.RetrieveAPIView):
    def get(self, request, format=None):
        queryset = Reserva.objects.all()
        serializer = ReservaSerializer(queryset, many=True)
        return Response(serializer.data)

class UltimaReservaView(generics.RetrieveAPIView):
    def get(self, request, iduser):
        queryset = Reserva.objects.all().select_related('habitacion').filter(user_id = iduser).order_by('-id')[:1]
        serializer = UltReservaSerializer(queryset, many=True)
        return Response(serializer.data)

class ListMaestroReservasView(generics.RetrieveAPIView):
    def get(self, request, format=None):
        queryset = Reserva.objects.all().select_related('habitacion__hotel', 'user').order_by('id')
        serializer = ListMaestReservasSerializer(queryset, many=True)
        return Response(serializer.data)