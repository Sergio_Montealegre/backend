from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .nuevoHotelView import NuevoHotelView
from .verHotelesView import VerHotelesView
from .nuevaHabitacionView import NuevaHabitacionView
from .verHabitacionesView import VerHabitacionesView
from .verHabitacionesView import HabitacionesHotelView
from .verHabitacionesView import HabitaDisponiblesView
from .nuevaReservaView import NuevaReservaView
from .verReservasView import VerReservasView
from .habReservadaView import HabReservadaView
from .verReservasView import UltimaReservaView
from .verReservasView import ListMaestroReservasView
from .cancelarReservaView import CancelarReservaView
from .actualInfoHabitView import actualValorHabitView
from .actualInfoHabitView import actualTipoHabitView
from .actualInfoHabitView import actualDescripHabitView
from .propioTokenView import PropioTokenView