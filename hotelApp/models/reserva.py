from django.db import models
from .habitacion import Habitacion
from .user import User

class Reserva(models.Model):
    id = models.AutoField(primary_key = True)
    fechaReserva = models.DateField()
    checkIn = models.DateField()
    checkOut = models.DateField()
    diasReserva = models.IntegerField()
    valorReserva = models.IntegerField()
    habitacion = models.ForeignKey(Habitacion, related_name="toReserva", on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name="fromReserva", on_delete=models.CASCADE)