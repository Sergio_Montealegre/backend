from hotelApp.models.reserva import Reserva
from rest_framework import serializers

class ReservaSerializer (serializers.ModelSerializer):
    class Meta:
        model = Reserva
        fields = ['id', 'fechaReserva', 'checkIn', 'checkOut', 'diasReserva',
                'valorReserva', 'habitacion', 'user']

class HabReservadaSerializer (serializers.ModelSerializer):
    class Meta:
        model = Reserva
        fields = ['habitacion_id', 'checkIn', 'checkOut']

class UltReservaSerializer (serializers.ModelSerializer):
    class Meta:
        model = Reserva

    def to_representation(self, instance):
        return {
            'habitacion': instance.habitacion.numeroHabitacion,
            'numeroPersonas': instance.habitacion.numeroPersonas,
            'tipoHabitacion': instance.habitacion.tipoHabitacion,
            'fechaReserva': instance.fechaReserva,
            'checkIn': instance.checkIn,
            'checkOut': instance.checkOut,
            'diasReserva': instance.diasReserva,
            'valorReserva': instance.valorReserva
        }

class ListMaestReservasSerializer (serializers.ModelSerializer):
    class Meta:
        model = Reserva

    def to_representation(self, instance):
        return {
            'id':instance.id,
            'fechaReserva': instance.fechaReserva,
            'cliente':instance.user.name,
            'hotel': instance.habitacion.hotel.nombreHotel,
            'habitacion': instance.habitacion.numeroHabitacion,
            'numeroPersonas': instance.habitacion.numeroPersonas,
            'checkIn': instance.checkIn,
            'checkOut': instance.checkOut,
            'diasReserva': instance.diasReserva,
            'valorReserva': instance.valorReserva
        }