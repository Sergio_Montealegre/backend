from rest_framework import generics
from rest_framework.response import Response

from hotelApp.models.hotel import Hotel 
from hotelApp.serializers.hotelSerializer import HotelSerializer

class VerHotelesView(generics.RetrieveAPIView):
    def get(self, request, format=None):
        queryset = Hotel.objects.all()
        serializer = HotelSerializer(queryset, many=True)
        return Response(serializer.data)