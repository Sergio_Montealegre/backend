from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.permissions import AllowAny
from hotelApp.serializers.propioTokenSerializer import PropioTokenSerializer

class PropioTokenView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = PropioTokenSerializer