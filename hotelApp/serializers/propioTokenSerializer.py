from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

class PropioTokenSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(PropioTokenSerializer, cls).get_token(user)

        # Aquí se agrega el campo de más que se necesita en el payload del Token
        token['rol'] = user.rol
        return token