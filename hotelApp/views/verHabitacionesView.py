from rest_framework import generics
from rest_framework.response import Response

from hotelApp.models.habitacion import Habitacion 
from hotelApp.serializers.habitacionSerializer import HabitacionSerializer
from hotelApp.serializers.habitacionSerializer import HabDisponSerializer
from hotelApp.models.reserva import Reserva 

class VerHabitacionesView(generics.RetrieveAPIView):
    def get(self, request, format=None):
        queryset = Habitacion.objects.all()
        serializer = HabitacionSerializer(queryset, many=True)
        return Response(serializer.data)

class HabitacionesHotelView(generics.RetrieveAPIView):
    def get(self, request, **kwargs):
        queryset = Habitacion.objects.filter(hotel = kwargs['idhotel'])
        serializer = HabitacionSerializer(queryset, many=True)
        return Response(serializer.data)

# Hacer aquí la consulta de todas las habitaciones disponibles
class HabitaDisponiblesView(generics.RetrieveAPIView):
    def get(self, request, idhotel, numper, checkin, checkout):
        queryset1 = Reserva.objects.values('habitacion_id').filter(checkOut__gte = checkin, checkIn__lte = checkout)
        queryset2 = Habitacion.objects.values('id', 'numeroHabitacion', 'tipoHabitacion','descripcion', 'valor').filter(hotel = idhotel, numeroPersonas__gte = numper).exclude(id__in=queryset1)        
        serializer = HabDisponSerializer(queryset2, many=True)
        return Response(serializer.data)