"""hotelProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django import urls
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from hotelApp import views
from django.contrib import admin

urlpatterns = [
    #path('login/', TokenObtainPairView.as_view()),
    path('admin/', admin.site.urls),
    path('login/', views.PropioTokenView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('nuevohotel/', views.NuevoHotelView.as_view()),
    path('verhoteles/', views.VerHotelesView.as_view()),
    path('nuevahabitacion/', views.NuevaHabitacionView.as_view()),
    path('verhabitaciones/', views.VerHabitacionesView.as_view()),
    path('verhabitaciones/<int:idhotel>/', views.HabitacionesHotelView.as_view()),
    path('habdisponibles/<int:idhotel>/<int:numper>/<checkin>/<checkout>', views.HabitaDisponiblesView.as_view()),
    path('nuevareserva/', views.NuevaReservaView.as_view()),
    path('verreservas/', views.VerReservasView.as_view()),
    path('habsreservadas/<checkin>/<checkout>', views.HabReservadaView.as_view()),
    path('listmaestroreservas/', views.ListMaestroReservasView.as_view()),
    path('ultimareserva/<int:iduser>/', views.UltimaReservaView.as_view()),
    path('cancelareserva/<int:idreserva>/', views.CancelarReservaView.as_view()),
    path('nuevovalorhab/<int:idhab>/<int:nuevovalor>/', views.actualValorHabitView.as_view()),
    path('nuevotipohab/<int:idhab>/<nuevotipo>/<int:personas>/', views.actualTipoHabitView.as_view()),
    path('nuevadescrhab/<int:idhab>/<nuevadescrip>/', views.actualDescripHabitView.as_view()),
]