from django.db import models
from .hotel import Hotel

class Habitacion(models.Model):
    id = models.IntegerField(primary_key = True)
    numeroHabitacion = models.IntegerField()
    numeroPersonas = models.IntegerField()
    tipoHabitacion = models.CharField(max_length = 45)
    descripcion = models.CharField(max_length = 200)
    valor = models.IntegerField()
    hotel = models.ForeignKey(Hotel, related_name="Habitacion", on_delete=models.CASCADE)