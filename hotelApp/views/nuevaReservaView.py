from rest_framework import status, views
from rest_framework.response import Response

from hotelApp.serializers.reservaSerializer import ReservaSerializer

class NuevaReservaView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = ReservaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)