from django.contrib import admin
from .models.user import User
from .models.hotel import Hotel
from .models.habitacion import Habitacion
from .models.reserva import Reserva

admin.site.register(User)
admin.site.register(Hotel)
admin.site.register(Habitacion)
admin.site.register(Reserva)
