from rest_framework import status, views
from rest_framework.response import Response

from hotelApp.models.habitacion import Habitacion 
from hotelApp.serializers.habitacionSerializer import HabitacionSerializer 

class actualValorHabitView(views.APIView):
    def put(self, request, idhab, nuevovalor):
        try:
            habitacion = Habitacion.objects.get(id=idhab)
        except Habitacion.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        habitacion.valor=nuevovalor
        habitacion.save()
        data = HabitacionSerializer(habitacion, many=False).data
        return Response(data=data, status=status.HTTP_200_OK)

class actualTipoHabitView(views.APIView):
    def put(self, request, idhab, nuevotipo, personas):
        try:
            habitacion = Habitacion.objects.get(id=idhab)
        except Habitacion.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        habitacion.tipoHabitacion=nuevotipo
        habitacion.numeroPersonas=personas
        habitacion.save()
        data = HabitacionSerializer(habitacion, many=False).data
        return Response(data=data, status=status.HTTP_200_OK)

class actualDescripHabitView(views.APIView):
    def put(self, request, idhab, nuevadescrip):
        try:
            habitacion = Habitacion.objects.get(id=idhab)
        except Habitacion.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        habitacion.descripcion=nuevadescrip
        habitacion.save()
        data = HabitacionSerializer(habitacion, many=False).data
        return Response(data=data, status=status.HTTP_200_OK)