from hotelApp.models.habitacion import Habitacion
from rest_framework import serializers

class HabitacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Habitacion
        fields = ['id', 'numeroHabitacion', 'numeroPersonas', 'tipoHabitacion',
                'descripcion', 'valor', 'hotel']

# Serializador de prueba
class HabDisponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Habitacion
        fields = ['id', 'numeroHabitacion', 'tipoHabitacion',
                'descripcion', 'valor']
