from django.db import models

class Hotel(models.Model):
    id = models.AutoField(primary_key = True)
    nombreHotel = models.CharField("Hotel", max_length = 45)
    ciudad = models.CharField("Ciudad", max_length = 45)
    direccion = models.CharField("Direccion", max_length = 45)
    correo = models.EmailField("Correo", max_length = 60)
    telefono = models.CharField("Telefono", max_length = 20)
    descripcion = models.TextField()